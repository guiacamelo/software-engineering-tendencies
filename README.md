# INF01063/CMP586 - Software Engeneering Tendencies Course   
UFRGS, Brazil, Lectured by Dr. Ingrid Nunes
Project developed by : Edson Andrade Filho, Guilherme Antonio Camelo, Lisandro Procedi.
In the final project of the course, we wanted to explore, and get to know Aspect Oriented Programming (AOP) só in this project we developed a simple hotel system with basic rent features in OOP and in AOP. After basic functions were created, a different programer added some aditional functions to evaluate how easy it wolud be to develop once the basic sistem were created.

In order to execute the AOP project, Visual Studio 2013 or higher with SpecFlow is needed. The files are in floder "tendenciasaop" 

In order to execute the OOP project, Visual Studio 2013 or higher with SheepAspect is needed. The files are in floder "tendencias"

The project report and the system were created in portuguese language, the project report can be seen in file "ProjectReport-DesenvolvimentoComparativoentrePOAePOO.pdf" 

BDD(Behaivor Driven Development) was used to document and perform automated testing with the help of SpecFlow. the system was created based on stories created by the users , here is an example of a story representeng the Login feature:

![login.png](https://bitbucket.org/repo/76x5Ka/images/1718827203-login.png)

 Technologies Used:

* * C#

* * Visual Studio 

* * SheepAspect

* * SpecFlow

* * BDD(Behaivor Driven Development)

![icon175x175.png](https://bitbucket.org/repo/76x5Ka/images/4025163885-icon175x175.png)

![visual-studio-purple-logo-595x335.jpg](https://bitbucket.org/repo/76x5Ka/images/2503278393-visual-studio-purple-logo-595x335.jpg)

![SheepAspect.0.2.0.1.jpg](https://bitbucket.org/repo/76x5Ka/images/2673788801-SheepAspect.0.2.0.1.jpg)

![specflow_logo.png](https://bitbucket.org/repo/76x5Ka/images/512054633-specflow_logo.png)