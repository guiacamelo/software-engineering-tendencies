﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tendencias.Dados;
using Tendencias.Negocio.Enums;

namespace Tendencias.Negocio
{
    public class AluguelQuarto
    {
        public AluguelQuarto()
        {
            Random random = new Random();
            this.DataInicio = DateTime.Now.AddDays(random.Next(1,10)*-1);
        }

        public string Cliente { get; set; }
        public int NumeroQuarto { get; set; }
        public Quarto Quarto { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }

        public void Inserir()
        {
            DaoAluguelQuarto dao = new DaoAluguelQuarto();

            if (String.IsNullOrEmpty(this.Cliente))
            {
                throw new IOException("O campo Cliente é obrigatório.");
            }

            if (this.NumeroQuarto == -1)
            {
                throw new IOException("O campo Número do apartamento é obrigatório.");
            }

            this.Quarto = Quarto.Consular(this.NumeroQuarto);

            if (this.Quarto == null)
            {
                throw new IOException("O número do quarto selecionado é inválido.");
            }
            else
            {
                if (this.Quarto.Status == StatusQuartoEnum.Ocupado)
                {
                    throw new IOException("O quarto " + this.NumeroQuarto.ToString() + " está ocupado.");
                }
                else if (this.Quarto.Status == StatusQuartoEnum.Reservado)
                {
                    var reserva = ReservaQuarto.Consultar(this.NumeroQuarto);
                    if(this.Cliente != reserva.Cliente)
                    throw new IOException("O quarto " + this.NumeroQuarto.ToString() + " está reservado.");
                }
            }

            this.Quarto.AlugarQuarto();
            dao.Inserir(this);
        }

        public void RegistrarLogCadastroQuarto(bool sucesso)
        {
            Log.RegistrarLog(CriarTextoLogCadastroAluguel(sucesso), @"LogCadastroAluguelQuarto.txt");
        }

        private string CriarTextoLogCadastroAluguel(bool sucesso)
        {
            StringBuilder builderLog = new StringBuilder();
            builderLog.Append("Tentativa de cadastro de aluguel de quarto ");
            builderLog.Append(DateTime.Now);
            builderLog.Append(Environment.NewLine);
            builderLog.Append("Status: ");
            builderLog.Append(sucesso ? "Válido" : "Inválido");
            builderLog.Append(Environment.NewLine);
            builderLog.Append("Cliente: ");
            builderLog.Append(this.Cliente);
            builderLog.Append(Environment.NewLine);
            builderLog.Append("Quarto: ");
            builderLog.Append(this.NumeroQuarto);
            builderLog.Append(Environment.NewLine);
            builderLog.Append("Data de Início: ");
            builderLog.Append(this.DataInicio);
            builderLog.Append(Environment.NewLine);
            builderLog.Append(Environment.NewLine);
            builderLog.Append(string.Empty.PadRight(100, '-'));
            builderLog.Append(Environment.NewLine);
            builderLog.Append(Environment.NewLine);
            return builderLog.ToString();
        }

        public void RegistrarLogCadastroAluguelQuartoSemPermissao()
        {
            Log.RegistrarLog(CriarTextoLogSemPermissaoAluguel(), @"LogCadastroAluguelQuarto.txt");
        }

        private string CriarTextoLogSemPermissaoAluguel()
        {
            StringBuilder builderLog = new StringBuilder();
            builderLog.Append("Tentativa de cadastro de aluguel de quato ");
            builderLog.Append(DateTime.Now);
            builderLog.Append(Environment.NewLine);
            builderLog.Append("Status: ");
            builderLog.Append("Sem permissão");
            builderLog.Append(Environment.NewLine);
            builderLog.Append(Environment.NewLine);
            builderLog.Append(string.Empty.PadRight(100, '-'));
            builderLog.Append(Environment.NewLine);
            builderLog.Append(Environment.NewLine);
            return builderLog.ToString();
        }

        public static AluguelQuarto Consultar(string nomeCliente)
        {
            DaoAluguelQuarto dao = new DaoAluguelQuarto();

            return dao.Consultar(nomeCliente);
        }

        public string CriarMensagemFinalizarAluguel()
        {
            TimeSpan date = DateTime.Today - this.DataInicio.Date;
            int diferencaDias = date.Days;
            decimal valorFinal = diferencaDias * this.Quarto.ValorDiaria;
            return String.Format("Quarto alugado desde {0}. Valor R${1}. Deseja finalizar o alguel?", this.DataInicio, valorFinal);
        }

        public void FinalizarAluguel()
        {
            this.DataFim = DateTime.Now;
            this.Quarto.Status = StatusQuartoEnum.Disponivel;
        }

        public void CriarLogFinalizacaoAluguel(bool finalizou)
        {
            Log.RegistrarLog(CriarTextoLogFinalizacaoAluguel(finalizou), @"LogFinzalicaoAluguel.txt");
        }

        private string CriarTextoLogFinalizacaoAluguel(bool finalizou)
        {
            StringBuilder builder = new StringBuilder();

            builder.Append("Tentativa de finalização do alguel ");
            builder.Append(DateTime.Now);
            builder.Append(Environment.NewLine);
            builder.Append("Quarto: ");
            builder.Append(this.Quarto.Numero);
            builder.Append(Environment.NewLine);
            builder.Append(finalizou ? "Finalizado" : "Não finalizado");
            builder.Append(Environment.NewLine);
            builder.Append(Environment.NewLine);
            builder.Append(string.Empty.PadRight(100, '-'));
            builder.Append(Environment.NewLine);
            builder.Append(Environment.NewLine);
            return builder.ToString();
        }

        public void RegistrarLogCadastroQuartoSemPermissao()
        {
            Log.RegistrarLog(CriarTextoLogSemPermissaoAluguel(), @"LogFinzalicaoAluguel.txt");
        }
    }
}
