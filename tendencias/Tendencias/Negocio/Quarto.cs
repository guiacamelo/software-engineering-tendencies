﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tendencias.Dados;
using Tendencias.Negocio.Enums;

namespace Tendencias.Negocio
{
    public class Quarto
    {
        public Quarto()
        {
            this.Status = StatusQuartoEnum.Disponivel;
        }

        public int Andar { get; set; }
        public int Numero { get; set; }
        public int Estrelas { get; set; }
        public decimal ValorDiaria { get; set; }
        public StatusQuartoEnum Status { get; set; }

        public void Inserir()
        {
            string mensagem = string.Empty;

            if(this.Andar == -1)
            {
                mensagem = "O campo Andar é obrigatório.";
            }
            if (this.Numero == -1)
            {
                mensagem = "O campo Número é obrigatório.";
            }
            if (this.Estrelas == -1)
            {
                mensagem = "O campo Estrelas é obrigatório.";
            }
            if (this.ValorDiaria == -1)
            {
                mensagem = "O campo Valor da diária é obrigatório.";
            }

            if (!String.IsNullOrEmpty(mensagem))
            {
                throw new IOException(mensagem);
            }

            DaoQuarto dao = new DaoQuarto();
            dao.Inserir(this);
        }

        public void RegistrarLogCadastroQuarto(bool sucesso)
        {
            Log.RegistrarLog(CriarTextoCadastroLog(sucesso), @"LogCadastroQuarto.txt");
        }

        public string CriarTextoCadastroLog(bool sucesso)
        {
            StringBuilder builderTextoLog = new StringBuilder();
            builderTextoLog.Append("Tentativa de cadastro de quarto ");
            builderTextoLog.Append(DateTime.Now);
            builderTextoLog.Append(Environment.NewLine);
            builderTextoLog.Append("Status: ");
            builderTextoLog.Append(sucesso ? "Válido" : "Inválido");
            builderTextoLog.Append(Environment.NewLine);
            builderTextoLog.Append("Andar: ");
            builderTextoLog.Append(this.Andar);
            builderTextoLog.Append(Environment.NewLine);
            builderTextoLog.Append("Número: ");
            builderTextoLog.Append(this.Numero);
            builderTextoLog.Append(Environment.NewLine);
            builderTextoLog.Append("Estrelas: ");
            builderTextoLog.Append(this.Estrelas);
            builderTextoLog.Append(Environment.NewLine);
            builderTextoLog.Append("Valor da diária: ");
            builderTextoLog.Append(this.ValorDiaria);
            builderTextoLog.Append(Environment.NewLine);
            builderTextoLog.Append(Environment.NewLine);
            builderTextoLog.Append(string.Empty.PadRight(100, '-'));
            builderTextoLog.Append(Environment.NewLine);
            builderTextoLog.Append(Environment.NewLine);
            return builderTextoLog.ToString();
        }

        public string CriarTextoCadastroLogSemPermissao()
        {
            StringBuilder builderTextoLog = new StringBuilder();
            builderTextoLog.Append("Tentativa de cadastro de quarto ");
            builderTextoLog.Append(DateTime.Now);
            builderTextoLog.Append(Environment.NewLine);
            builderTextoLog.Append("Status: ");
            builderTextoLog.Append("Sem permissão");
            builderTextoLog.Append(Environment.NewLine);
            builderTextoLog.Append(Environment.NewLine);
            builderTextoLog.Append(string.Empty.PadRight(100, '-'));
            builderTextoLog.Append(Environment.NewLine);
            builderTextoLog.Append(Environment.NewLine);
            return builderTextoLog.ToString();
        }

        public void RegistrarLogCadastroQuartoSemPermissao()
        {
            Log.RegistrarLog(CriarTextoCadastroLogSemPermissao(), @"LogCadastroQuarto.txt");
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(this.Andar);
            builder.Append("º andar - Número ");
            builder.Append(this.Numero);
            builder.Append(" - ");
            switch(this.Status)
            {
                case StatusQuartoEnum.Disponivel:
                    builder.Append("Disponível");
                    break;
                case StatusQuartoEnum.Ocupado:
                    builder.Append("Ocupado");
                    break;
                default:
                    builder.Append("Reservado");
                    break;
            }
            
            return builder.ToString();
        }

        public static List<string> ListaQuartosParaAlugar()
        {
            DaoQuarto dao = new DaoQuarto();
            return dao.ListaQuartosParaAlugar();
        }

        public void ReservarQuarto()
        {
            this.Status = StatusQuartoEnum.Reservado;
        }

        public void AlugarQuarto()
        {
            this.Status = StatusQuartoEnum.Ocupado;
        }

        public static Quarto Consular(int numero)
        {
            DaoQuarto dao = new DaoQuarto();
            return dao.Consultar(numero);
        }
    }
}
