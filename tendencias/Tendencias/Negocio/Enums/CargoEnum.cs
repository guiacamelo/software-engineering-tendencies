﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tendencias.Negocio.Enums
{
    public enum CargoEnum
    {
        FuncionarioLimpeza = 1,
        FuncionarioRecepcao,
        Gerente
    }
}
