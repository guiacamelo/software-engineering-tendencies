﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tendencias.Dados;
using Tendencias.Negocio.Enums;

namespace Tendencias.Negocio
{
    public class ReservaQuarto
    {
        public ReservaQuarto()
        {

        }
        public string Cliente { get; set; }
        public int NumeroQuarto { get; set; }
        public Quarto Quarto { get; set; }

        public void Inserir()
        {
            DaoReservaQuarto dao = new DaoReservaQuarto();

            if(String.IsNullOrEmpty(this.Cliente))
            {
                throw new IOException("O campo Cliente é obrigatório.");
            }

            if(this.NumeroQuarto == -1)
            {
                throw new IOException("O campo Número do apartamento é obrigatório.");
            }

            this.Quarto = Quarto.Consular(this.NumeroQuarto);

            if (this.Quarto == null)
            {
                throw new IOException("O número do quarto selecionado é inválido.");
            }
            else
            {
                if (this.Quarto.Status == StatusQuartoEnum.Ocupado)
                {
                    throw new IOException("O quarto " + this.NumeroQuarto.ToString() + " está ocupado.");
                }
                else if (this.Quarto.Status == StatusQuartoEnum.Reservado)
                {
                    throw new IOException("O quarto " + this.NumeroQuarto.ToString() + " está reservado.");
                }
            }

            this.Quarto.ReservarQuarto();
            dao.Inserir(this);
        }

        private string CriarTextoLogCadastroReserva(bool sucesso)
        {
            StringBuilder builderLog = new StringBuilder();
            builderLog.Append("Tentativa de cadastro de reserva de aluguel ");
            builderLog.Append(DateTime.Now);
            builderLog.Append(Environment.NewLine);
            builderLog.Append("Status: ");
            builderLog.Append(sucesso ? "Válido" : "Inválido");
            builderLog.Append(Environment.NewLine);
            builderLog.Append("Cliente: ");
            builderLog.Append(this.Cliente);
            builderLog.Append(Environment.NewLine);
            builderLog.Append("Quarto: ");
            builderLog.Append(this.NumeroQuarto);
            builderLog.Append(Environment.NewLine);
            builderLog.Append(Environment.NewLine);
            builderLog.Append(string.Empty.PadRight(100, '-'));
            builderLog.Append(Environment.NewLine);
            builderLog.Append(Environment.NewLine);
            return builderLog.ToString();
        }

        private string CriarTextoLogSemPermissaoReserva()
        {
            StringBuilder builderLog = new StringBuilder();
            builderLog.Append("Tentativa de cadastro de reserva de quarto ");
            builderLog.Append(DateTime.Now);
            builderLog.Append(Environment.NewLine);
            builderLog.Append("Status: ");
            builderLog.Append("Sem permissão");
            builderLog.Append(Environment.NewLine);
            builderLog.Append(Environment.NewLine);
            builderLog.Append(string.Empty.PadRight(100, '-'));
            builderLog.Append(Environment.NewLine);
            builderLog.Append(Environment.NewLine);
            return builderLog.ToString();
        }

        public void RegistrarLogCadastroQuarto(bool sucesso)
        {
            Log.RegistrarLog(CriarTextoLogCadastroReserva(sucesso), @"LogCadastroReservaQuarto.txt");
        }

        public void RegistrarLogCadastroReservaQuartoSemPermissao()
        {
            Log.RegistrarLog(CriarTextoLogSemPermissaoReserva(), @"LogCadastroReservaQuarto.txt");
        }

        public static ReservaQuarto Consultar(int numeroQuarto)
        {
            DaoReservaQuarto dao = new DaoReservaQuarto();

            return dao.Consultar(numeroQuarto);
        }

        public string CriarMensagemRemoverReserva()
        {
            return String.Format("Quarto {0}. Deseja remover reserva?", this.NumeroQuarto);
        }

        public static ReservaQuarto Consultar(string nome)
        {
            DaoReservaQuarto dao = new DaoReservaQuarto();

            return dao.Consultar(nome);
        }

        public void RemoverReserva()
        {
            DaoReservaQuarto dao = new DaoReservaQuarto();
            dao.RemoverReserva(this);
            this.Quarto.Status = StatusQuartoEnum.Disponivel;
        }

        private string CriarLogRemoverReserva(string status)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("Tentativa de remoção de reserva ");
            builder.Append(DateTime.Now);
            builder.Append(Environment.NewLine);
            builder.Append("Status: ");
            builder.Append(status);
            builder.Append(Environment.NewLine);
            builder.Append(Environment.NewLine);
            builder.Append(string.Empty.PadRight(100, '-'));
            builder.Append(Environment.NewLine);
            builder.Append(Environment.NewLine);
            return builder.ToString();
        }

        public void RegistraLogRemocaoReserva(string status)
        {
            Log.RegistrarLog(CriarLogRemoverReserva(status), @"LogRemocaoReserva.txt");
        }
    }
}
