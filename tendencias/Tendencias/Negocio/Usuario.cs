﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tendencias.Dados;
using Tendencias.Negocio.Enums;
using System.IO;

namespace Tendencias.Negocio
{
    public class Usuario
    {
        public Usuario()
        {

        }

        public Usuario(string nome, string login, string senha, CargoEnum cargo)
        {
            this.Nome = nome;
            this.Login = login;
            this.Senha = senha;
            this.Cargo = cargo;
        }

        public string Nome { get; set; }
        public string Login { get; set; }
        public string Senha { get; set; }
        public CargoEnum? Cargo { get; set; }

        public static Usuario ValidaLoginSenha(Tuple<string, string> tuplaLoginSenha)
        {
            DaoUsuario dao = new DaoUsuario();
            return dao.ValidaLoginSenha(tuplaLoginSenha);
        }

        public static void CriarLogLoginInvalido()
        {
            Log.RegistrarLog(CriaTextoLogLoginInvalido(), @"LogLogin.txt");
        }

        public void CriaLogLogin()
        {
            Log.RegistrarLog(CriaTextoLogLoginValido(), @"LogLogin.txt");
        }

        private static string CriaTextoLogLoginInvalido()
        {
            StringBuilder builderLog = new StringBuilder();
            builderLog.Append("Tentativa de login ");
            builderLog.Append(DateTime.Now);
            builderLog.Append(Environment.NewLine);
            builderLog.Append("Status: Inválido");
            builderLog.Append(Environment.NewLine);
            builderLog.Append(Environment.NewLine);
            builderLog.Append(string.Empty.PadRight(100, '-'));
            builderLog.Append(Environment.NewLine);
            builderLog.Append(Environment.NewLine);
            return builderLog.ToString();
        }

        private string CriaTextoLogLoginValido()
        {
            StringBuilder builderLog = new StringBuilder();
            builderLog.Append("Tentativa de log ");
            builderLog.Append(DateTime.Now);
            builderLog.Append(Environment.NewLine);
            builderLog.Append("Status: Válido");
            builderLog.Append(Environment.NewLine);
            builderLog.Append("Nome: ");
            builderLog.Append(this.Nome);
            builderLog.Append(Environment.NewLine);
            builderLog.Append(Environment.NewLine);
            builderLog.Append(string.Empty.PadRight(100, '-'));
            builderLog.Append(Environment.NewLine);
            builderLog.Append(Environment.NewLine);
            return builderLog.ToString();
        }

        public void RegistrarLogTentativaCadastroSemPermissao()
        {
            Log.RegistrarLog(CriarTextoLogCadastroSemPermissao(), @"LogCadastroUsuario.txt");
        }


        public void Inserir()
        {
            if(String.IsNullOrEmpty(this.Nome))
            {
                throw new IOException("O campo Nome é obrigatório.");
            }
            if (String.IsNullOrEmpty(this.Login))
            {
                throw new IOException("O campo Login é obrigatório.");
            }
            if (String.IsNullOrEmpty(this.Senha))
            {
                throw new IOException("O campo Senha é obrigatório.");
            }
            if (!Cargo.HasValue)
            {
                throw new IOException("O campo Cargo é obrigatório.");
            }

            DaoUsuario dao = new DaoUsuario();
            dao.Inserir(this);
        }

        public void RegistrarLogTentativaCadastro(bool sucesso)
        {
            Log.RegistrarLog(CriarTextoLogCadastro(sucesso), @"LogCadastroUsuario.txt");
        }

        private string CriarTextoLogCadastro(bool sucesso)
        {
            StringBuilder builderLog = new StringBuilder();    
            builderLog.Append("Tentativa de cadastro de usuário ");
            builderLog.Append(DateTime.Now);
            builderLog.Append(Environment.NewLine);
            builderLog.Append("Status: ");
            builderLog.Append(sucesso ? "Válido" : "Inválido");
            builderLog.Append(Environment.NewLine);
            builderLog.Append("Nome: ");
            builderLog.Append(this.Nome);
            builderLog.Append(Environment.NewLine);
            builderLog.Append("Login: ");
            builderLog.Append(this.Login);
            builderLog.Append(Environment.NewLine);
            builderLog.Append("Senha: ");
            builderLog.Append(this.Senha);
            builderLog.Append(Environment.NewLine);
            builderLog.Append("Cargo: ");
            builderLog.Append(this.Cargo == null ? string.Empty : this.Cargo.ToString());
            builderLog.Append(Environment.NewLine);
            builderLog.Append(Environment.NewLine);
            builderLog.Append(string.Empty.PadRight(100, '-'));
            builderLog.Append(Environment.NewLine);
            builderLog.Append(Environment.NewLine);
            return builderLog.ToString();
        }

        public string CriarTextoLogCadastroSemPermissao()
        {
            StringBuilder builderLog = new StringBuilder();
            builderLog.Append("Tentativa de cadastro de usuário ");
            builderLog.Append(DateTime.Now);
            builderLog.Append(Environment.NewLine);
            builderLog.Append("Status: ");
            builderLog.Append("Usuário sem permissão");
            builderLog.Append(Environment.NewLine);
            builderLog.Append(Environment.NewLine);
            builderLog.Append(string.Empty.PadRight(100, '-'));
            builderLog.Append(Environment.NewLine);
            builderLog.Append(Environment.NewLine);
            return builderLog.ToString();
        }
    }
}
