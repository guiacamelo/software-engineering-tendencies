﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tendencias.Apresentacao.Enums
{
    public enum OpcaoMenuEnum
    {
        CadastrarUsuario = 1,
        CadastrarQuarto,
        ReservarQuarto,
        AlugarQuarto,
        FinalizarAluguel,
        RemoverReserva,
        Sair
    }
}
