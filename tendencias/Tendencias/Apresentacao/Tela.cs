﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tendencias.Apresentacao.Enums;
using Tendencias.Negocio;
using Tendencias.Negocio.Enums;

namespace Tendencias.Apresentacao
{
    public static class Tela
    {
        public static Usuario usuarioLogado;

        public static void ApresentaTelaInicial()
        {
            Console.WriteLine("Olá, Bem vindo ao sistema.\n");
        }

        public static void LimpaTela()
        {
            Console.Clear();
        }

        public static Tuple<string, string> LeDadosLogin()
        {
            string login, senha;
            Console.Write("Login: ");
            login = Console.ReadLine();
            Console.Write("Senha: ");
            senha = Console.ReadLine();

            return new Tuple<string, string>(login, senha);
        }

        public static void ApresentaMensagemErroLogin()
        {
            LimpaTela();
            Console.WriteLine("### Login ou senha inválidos ###");
            Console.ReadLine();
            LimpaTela();
        }

        public static void ApresentarMenuPrincipal()
        {
            LimpaTela();
            Console.WriteLine("Olá, {0}", usuarioLogado.Nome);
            Console.WriteLine(Environment.NewLine);
            Console.WriteLine((int)OpcaoMenuEnum.CadastrarUsuario + " - Cadastrar Usuário");
            Console.WriteLine((int)OpcaoMenuEnum.CadastrarQuarto + " - Cadastrar Quarto");
            Console.WriteLine((int)OpcaoMenuEnum.ReservarQuarto + " - Reservar Quarto");
            Console.WriteLine((int)OpcaoMenuEnum.AlugarQuarto + " - Alugar Quarto");
            Console.WriteLine((int)OpcaoMenuEnum.FinalizarAluguel + " - Finalizar Aluguel");
            Console.WriteLine((int)OpcaoMenuEnum.RemoverReserva + " - Remover Reserva");
            Console.WriteLine(Environment.NewLine);
        }

        public static int LerOpcaoMenu()
        {
            Console.Write("Informe a opção deseja: ");
            return Convert.ToInt32(Console.ReadLine());
        }

        public static void ApresentarMenuSelecionado(int opcao)
        {
            switch (opcao)
            {
                case (int)OpcaoMenuEnum.CadastrarUsuario:
                    ApresentaMenuCadastrarUsuario();
                    break;
                case (int)OpcaoMenuEnum.CadastrarQuarto:
                    ApresentarMenuCadastrarQuarto();
                    break;
                case (int)OpcaoMenuEnum.ReservarQuarto:
                    ApresentarMenuReservarQuarto();
                    break;
                case (int)OpcaoMenuEnum.AlugarQuarto:
                    ApresentarMenuAlugarQuarto();
                    break;
                case (int)OpcaoMenuEnum.FinalizarAluguel:
                    ApresentarMenuFinalizarAluguel();
                    break;
                case (int)OpcaoMenuEnum.RemoverReserva:
                    ApresentarMenuRemoverReserva();
                    break;
            }
        }

        private static void ApresentarMenuAlugarQuarto()
        {
            bool sucesso = false;
            AluguelQuarto aluguel = new AluguelQuarto();
            LimpaTela();

            if (usuarioLogado.Cargo != CargoEnum.Gerente && usuarioLogado.Cargo != CargoEnum.FuncionarioRecepcao)
            {
                aluguel.RegistrarLogCadastroAluguelQuartoSemPermissao();
                LimpaTela();
                Console.WriteLine("*** Para acessar essa página você deve ter o cargo de gerente ou de funcionário da recepção. ***\n ***  Será redirecionado para a tela incial. ***");
                Console.ReadLine();
                ApresentarMenuPrincipal();
                return;
            }
            Console.WriteLine("*** Alugar quarto ***");
            var listaQuartos = Quarto.ListaQuartosParaAlugar();
            foreach (string s in listaQuartos)
            {
                Console.WriteLine(s);
            }
            Console.WriteLine();
            Console.Write("Cliente: ");
            aluguel.Cliente = Console.ReadLine();
            Console.Write("Número do quarto: ");
            try
            {
                aluguel.NumeroQuarto = Convert.ToInt32(Console.ReadLine());
            }
            catch
            {
                aluguel.NumeroQuarto = -1;
            }
            try
            {
                aluguel.Inserir();
                sucesso = true;
            }
            catch (IOException e)
            {
                LimpaTela();
                Console.WriteLine("***" + e.Message + "***");
                Console.ReadLine();
                sucesso = false;
            }
            finally
            {
                aluguel.RegistrarLogCadastroQuarto(sucesso);
            }
        }

        private static void ApresentarMenuReservarQuarto()
        {
            bool sucesso = false;
            ReservaQuarto reserva = new ReservaQuarto();
            LimpaTela();

            if (usuarioLogado.Cargo != CargoEnum.Gerente && usuarioLogado.Cargo != CargoEnum.FuncionarioRecepcao)
            {
                reserva.RegistrarLogCadastroReservaQuartoSemPermissao();
                LimpaTela();
                Console.WriteLine("*** Para acessar essa página você deve ter o cargo de gerente ou de funcionário da recepção. ***\n *** Será redirecionado para a tela incial. ***");
                Console.ReadLine();
                ApresentarMenuPrincipal();
                return;
            }
            Console.WriteLine("*** Reservar quarto ***");
            var listaQuartos = Quarto.ListaQuartosParaAlugar();
            foreach(string s in listaQuartos)
            {
                Console.WriteLine(s);
            }
            Console.WriteLine();
            Console.Write("Cliente: ");
            reserva.Cliente = Console.ReadLine();
            Console.Write("Número do quarto: ");
            try
            {
                reserva.NumeroQuarto = Convert.ToInt32(Console.ReadLine());
            }
            catch
            {
                reserva.NumeroQuarto = -1;
            }
            try
            {
                reserva.Inserir();
                sucesso = true;
            }
            catch (IOException e)
            {
                LimpaTela();
                Console.WriteLine("***" + e.Message + "***");
                Console.ReadLine();
                sucesso = false;
            }
            finally
            {
                reserva.RegistrarLogCadastroQuarto(sucesso);
            }
        }

        private static void ApresentarMenuCadastrarQuarto()
        {
            bool sucesso = false;
            Quarto quarto = new Quarto();

            LimpaTela();
            if (usuarioLogado.Cargo != CargoEnum.Gerente && usuarioLogado.Cargo != CargoEnum.FuncionarioRecepcao)
            {
                quarto.RegistrarLogCadastroQuartoSemPermissao();
                LimpaTela();
                Console.WriteLine("*** Para acessar essa página você deve ter o cargo de gerente ou de funcionário da recepção. ***\n *** Será redirecionado para a tela incial. ***");
                Console.ReadLine();
                ApresentarMenuPrincipal();
                return;
            }

            Console.WriteLine("*** Cadastrar Quarto ***\n\n");
            Console.Write("Andar: ");
            try
            {
                quarto.Andar = Convert.ToInt32(Console.ReadLine());
            }
            catch
            {
                quarto.Andar = -1;
            }
            Console.Write("Número: ");
            try
            {
                quarto.Numero = Convert.ToInt32(Console.ReadLine());
            }
            catch
            {
                quarto.Numero = -1;
            }
            Console.Write("Estrelas: ");
            try
            {
                quarto.Estrelas = Convert.ToInt32(Console.ReadLine());
            }
            catch
            {
                quarto.Estrelas = -1;
            }
            Console.Write("Valor da diária: ");
            try
            {
                quarto.ValorDiaria = Convert.ToDecimal(Console.ReadLine());
            }
            catch
            {
                quarto.ValorDiaria = -1;
            }

            try
            {
                quarto.Inserir();
                sucesso = true;
            }
            catch(IOException e)
            {
                LimpaTela();
                Console.WriteLine("***" + e.Message + "***");
                Console.ReadLine();
                sucesso = false;
            }
            finally
            {
                quarto.RegistrarLogCadastroQuarto(sucesso);
                LimpaTela();
            }
        }

        private static void ApresentaMenuCadastrarUsuario()
        {
            bool sucesso = false;
            Usuario usuario = new Usuario();
            if (usuarioLogado.Cargo != CargoEnum.Gerente)
            {
                usuario.RegistrarLogTentativaCadastroSemPermissao();
                LimpaTela();
                Console.WriteLine("*** Para acessar essa tela você deve ter o cargo de gerente. *** \n *** Será redirecionado para a tela incial. ***");
                Console.ReadLine();
                ApresentarMenuPrincipal();
                return;
            }

            try
            {
                LimpaTela();
                int cargo;
                Console.WriteLine("*** Cadastrar Usuário ***\n\n");
                Console.Write("Nome: ");
                usuario.Nome = Console.ReadLine();
                Console.Write("Login: ");
                usuario.Login = Console.ReadLine();
                Console.Write("Senha: ");
                usuario.Senha = Console.ReadLine();
                Console.WriteLine("Cargos:");
                Console.WriteLine((int)CargoEnum.FuncionarioLimpeza + " - Funcionário da Limpeza");
                Console.WriteLine((int)CargoEnum.FuncionarioRecepcao + " - Funcionário da Recepção");
                Console.WriteLine((int)CargoEnum.Gerente + " - Gerente");
                Console.Write("Selecione o cargo: ");
                try
                {
                    cargo = Convert.ToInt32(Console.ReadLine());
                }
                catch
                {
                    cargo = -1;
                }
                switch (cargo)
                {
                    case (int)CargoEnum.FuncionarioLimpeza:
                        usuario.Cargo = CargoEnum.FuncionarioLimpeza;
                        break;
                    case (int)CargoEnum.FuncionarioRecepcao:
                        usuario.Cargo = CargoEnum.FuncionarioRecepcao;
                        break;
                    case (int)CargoEnum.Gerente:
                        usuario.Cargo = CargoEnum.Gerente;
                        break;
                    default:
                        usuario.Cargo = null;
                        break;
                }
                usuario.Inserir();
                sucesso = true;
            }
            catch (IOException e)
            {
                LimpaTela();
                Console.WriteLine("***" + e.Message + "***");
                Console.ReadLine();
                ApresentarMenuPrincipal();
                sucesso = false;
            }
            finally
            {
                usuario.RegistrarLogTentativaCadastro(sucesso);
                ApresentarMenuPrincipal();
            }
        }

        public static void ApresentarMenuFinalizarAluguel()
        {
            string cliente;
            string resposta;
            var aluguel = new AluguelQuarto();

            LimpaTela();
            if (usuarioLogado.Cargo != CargoEnum.Gerente && usuarioLogado.Cargo != CargoEnum.FuncionarioRecepcao)
            {
                aluguel = new AluguelQuarto();
                aluguel.RegistrarLogCadastroQuartoSemPermissao();
                LimpaTela();
                Console.WriteLine("*** Para acessar essa página você deve ter o cargo de gerente ou de funcionário da recepção. *** \n *** Será redirecionado para a tela incial. ***");
                Console.ReadLine();
                ApresentarMenuPrincipal();
                return;
            }

            LimpaTela();
            Console.WriteLine("*** Finaliza Aluguel ***");

            Console.Write("Cliente: ");
            cliente = Console.ReadLine();

            if(String.IsNullOrEmpty(cliente))
            {
                Console.WriteLine("*** O campo Cliente é obrigatório. ***");
                return;
            }


            aluguel = AluguelQuarto.Consultar(cliente);
            Console.WriteLine();
            Console.WriteLine(aluguel.CriarMensagemFinalizarAluguel());
            Console.WriteLine();
            Console.Write("S para sim ou N para não: ");
            resposta = Console.ReadLine();
            if(resposta.Equals("s"))
            {
                aluguel.FinalizarAluguel();
                aluguel.CriarLogFinalizacaoAluguel(true);
            }
            else
            {
                aluguel.CriarLogFinalizacaoAluguel(false);
            }
        }

        public static void ApresentarMenuRemoverReserva()
        {
            
            ReservaQuarto reserva = new ReservaQuarto();
            string nome = string.Empty;
            string opcao;
            LimpaTela();

            if (usuarioLogado.Cargo != CargoEnum.Gerente && usuarioLogado.Cargo != CargoEnum.FuncionarioRecepcao)
            {
                reserva.RegistraLogRemocaoReserva("Sem Permissão");
                LimpaTela();
                Console.WriteLine("*** Para acessar essa página você deve ter o cargo de gerente ou de funcionário da recepção. *** \n *** Será redirecionado para a tela incial. ***");
                Console.ReadLine();
                return;
            }

            Console.WriteLine("*** Remover Reserva ***\n");

            Console.Write("Nome: ");
            nome = Console.ReadLine();

            if(String.IsNullOrEmpty(nome))
            {
                Console.WriteLine("*** O campo Nome é obrigatório. ***");
                reserva.RegistraLogRemocaoReserva("Inválido");
                return;
            }

            reserva = ReservaQuarto.Consultar(nome);

            if(reserva == null)
            {
                Console.WriteLine("*** Nome inválido. ***");
                reserva = new ReservaQuarto();
                reserva.RegistraLogRemocaoReserva("Inválido");
                return;
            }

            Console.WriteLine(reserva.CriarMensagemRemoverReserva());
            Console.Write("S para sim, N para não: ");
            opcao = Console.ReadLine();

            if (opcao.ToUpper().Equals("S"))
            {
                reserva.RemoverReserva();
                reserva.RegistraLogRemocaoReserva("Válido");
            }
            else
            {
                reserva = new ReservaQuarto();
                reserva.RegistraLogRemocaoReserva("Cancelado");
            }
        }
    }
}
