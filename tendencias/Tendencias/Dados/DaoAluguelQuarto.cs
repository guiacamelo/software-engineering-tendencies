﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tendencias.Negocio;

namespace Tendencias.Dados
{
    public class DaoAluguelQuarto
    {
        public DaoAluguelQuarto()
        {

        }

        public void Inserir(AluguelQuarto aluguel)
        {
            Banco.AlugueisQuarto.Add(aluguel);
        }

        public AluguelQuarto Consultar(string nomeCliente)
        {
            return Banco.AlugueisQuarto.SingleOrDefault(x => x.Cliente == nomeCliente);
        }
    }
}
