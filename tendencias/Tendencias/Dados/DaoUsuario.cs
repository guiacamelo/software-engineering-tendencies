﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tendencias.Negocio;

namespace Tendencias.Dados
{
    public class DaoUsuario
    {
        public Usuario ValidaLoginSenha(Tuple<string, string> tuplaLoginSenha)
        {
            var usuario = (from u in Banco.Usuarios
                           where u.Login == tuplaLoginSenha.Item1 &&
                                 u.Senha == tuplaLoginSenha.Item2
                            select u).FirstOrDefault();

            return usuario;
        }

        public void Inserir(Usuario usuario)
        {
            Banco.Usuarios.Add(usuario);
        }
    }
}
