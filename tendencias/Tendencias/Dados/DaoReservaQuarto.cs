﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tendencias.Negocio;

namespace Tendencias.Dados
{
    public class DaoReservaQuarto
    {
        public DaoReservaQuarto()
        {

        }

        public void Inserir(ReservaQuarto reservaQuarto)
        {
            Banco.ReservasQuarto.Add(reservaQuarto);
        }

        public ReservaQuarto Consultar(int numeroQuarto)
        {
            return Banco.ReservasQuarto.Single(x => x.NumeroQuarto == numeroQuarto);
        }

        public ReservaQuarto Consultar(string nome)
        {
            return Banco.ReservasQuarto.SingleOrDefault(x => x.Cliente == nome);
        }

        public void RemoverReserva(ReservaQuarto reservaQuarto)
        {
            Banco.ReservasQuarto.Remove(reservaQuarto);
        }
    }
}
