﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tendencias.Negocio;

namespace Tendencias.Dados
{
    public static class Banco
    {
        public static void IniciaBanco()
        {
            Usuarios = new List<Usuario>()
            {
                new Usuario("João da Silva", "joao", "joao", Negocio.Enums.CargoEnum.Gerente),
                new Usuario("Francisco Cabral", "chico", "chico", Negocio.Enums.CargoEnum.FuncionarioRecepcao),
                new Usuario("Juca Bala", "juca", "juca", Negocio.Enums.CargoEnum.FuncionarioLimpeza)
            };
            Quartos = new List<Quarto>();
            ReservasQuarto = new List<ReservaQuarto>();
            AlugueisQuarto = new List<AluguelQuarto>();
        }
        public static List<Usuario> Usuarios;
        public static List<Quarto> Quartos;
        public static List<ReservaQuarto> ReservasQuarto;
        public static List<AluguelQuarto> AlugueisQuarto;
    }
}
