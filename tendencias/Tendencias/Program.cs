﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tendencias.Apresentacao;
using Tendencias.Dados;
using Tendencias.Negocio;

namespace Tendencias
{
    class Program
    {
        static void Main(string[] args)
        {
            int opcaoMenu;
            Banco.IniciaBanco();
            Tuple<string, string> tuplaLoginSenha;
            do
            {
                Tela.ApresentaTelaInicial();
                tuplaLoginSenha = Tela.LeDadosLogin();
                Tela.usuarioLogado = Usuario.ValidaLoginSenha(tuplaLoginSenha);
                if(Tela.usuarioLogado == null)
                {
                    Tela.ApresentaMensagemErroLogin();
                    Usuario.CriarLogLoginInvalido();
                }

            } while (Tela.usuarioLogado == null);
            Tela.usuarioLogado.CriaLogLogin();

            do
            {
                Tela.ApresentarMenuPrincipal();
                opcaoMenu = Tela.LerOpcaoMenu();
                Tela.ApresentarMenuSelecionado(opcaoMenu);
            } while (true);
        }
    }
}
