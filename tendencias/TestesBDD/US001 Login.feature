﻿Funcionalidade: US001 Login
	Eu como usuário do sistema
	Quero fazer login
	Para que eu possa acessar as funcionalidades do sistema

Contexto:
Dado que tenho os seguintes usuários cadastrados no sistema
| Nome      | Login    | Senha       | Cargo                   |
| Ivanildo  | ivanildo | 12345       | Funcionário da recepção |
| Juvenildo | juve     | juve        | Funcionário da limpeza  |
| Guilherme | camelo   | prestobarba | Gerente                 |
| Lisandro  | cabelo   | safadao     | Gerente                 |
E que estou na tela de login

Cenário: Deve fazer login informando login e usuário válido
Dado que preencho o Login e a Senha com valores válidos de um usuário
Quando terminar de preencher os campos
Então os valores serão validados
E vou para a tela inicial
E um log de tentativa de acesso ao sistema é incluído

Cenário: Deve mostrar mensagem para login ou senha inválidos
Dado que preencho o Login e Senha com valores inválidos
Quando terminar de preencher os campos
Então os valores serão validados
E uma mensagem de validação é apresentada "Usuário ou senha inválidos."
E continua na tela de login
E um log de tentativa de acesso ao sistema é incluído

