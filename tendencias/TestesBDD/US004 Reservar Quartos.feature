﻿Funcionalidade: US004 Reservar Quartos
	Eu como gerente ou funcionário da recepção
	Quero reservar quartos
	Para que os clientes possam ter garantia de que o quarto que queiram alugar esteja disponível

Contexto: 
Dado tenho os seguintes quartos
| Andar | Número | Estrelas | Situação  |
| 1     | 101    | 5        | Vago      |
| 1     | 102    | 5        | Ocupado   |
| 2     | 201    | 4        | Reservado |
| 2     | 202    | 4        | Vago      |
E que estou na tela inicial

Cenário: Deve reservar um quarto do hotel
Dado que sou um usuário com cargo de gerente ou de funcionário da recepção
E que acesso a tela de reserva de quarto
E que os quartos são listados
E que informo o nome do cliente
Quando seleciono o quarto 101
Então o quarto fica resevado
E uma mensagem de sucesso é apresentada "Quarto 101 reservado com sucesso."
E volta para a tela inicial
E um log de reserva de quarto é incluído

Cenário: Deve impedir a reserva de um quarto reservado
Dado que sou um usuário com cargo de gerente ou de funcionário da recepção
E que acesso a tela de reserva de quarto
E que os quartos são listados
E que informo o nome do cliente
Quando seleciono o quarto 201
Então uma mensagem de validação é apresentada "Quarto já reservado."
E volta para a tela inicial
E um log de reserva de quarto é incluído

Cenário: Deve impedir a reserva de um quarto ocupado
Dado que sou um usuário com cargo de gerente ou de funcionário da recepção
E que acesso a tela de reserva de quarto
E que os quartos são listados
E que informo o nome do cliente
Quando seleciono o quarto 102
Então uma mensagem de validação é apresentada "Quarto já ocupado."
E volta para a tela inicial
E um log de reserva de quarto é incluído

Cenário: Deve impedir a reserva de um quarto com número inválido
Dado que sou um usuário com cargo de gerente ou de funcionário da recepção
E que acesso a tela de reserva de quarto
E que os quartos são listados
E que informo o nome do cliente
Quando seleciono o quarto 999
Então uma mensagem de validação é apresentada "O quarto selecionado é inválido."
E volta para a tela inicial
E um log de reserva de quarto é incluído

Esquema do Cenário: Deve validar os campos obrigatórios para reservar um quarto
Dado que sou um usuário com cargo de gerente ou de funcionário da recepção
E que acesso a tela de reserva de quarto
E que os quartos são listados
E que não informo o campo <campo>
Quando terminar de preencher os campos
Então uma mensagem de validação é apresentada <mensagem>
E volta para a tela inicial
E um log de reserva de quarto é incluído
Exemplos: 
| campo      | mensagem                           |
| Cliente    | O campo Cliente é obrigatório.     |
| Apartamnto | O campo Apartamento é obrigatório. |

Cenário: Deve bloquear a entrada de usuário diferente de gerente ou funcionário da recepção na tela de reserva de quartos
Dado que não sou um usuário com cargo de gerente ou de funcionário da recepção
Quando acesso a tela de reservar quartos
Então uma mensagem de validação é apresentada "Para acessar essa página você deve ter o cargo de gerente ou de funcionário da recepção. Será redirecionado para a tela incial."
E volto para a tela inicial
E um log de reserva de quarto é incluído


