﻿Funcionalidade: US007 Remover Reserva
	Eu como gerente ou funcionário da recepção
	Quero remover as reservas de quartos
	Para que clientes não queiram mais alugar os quartos possam remover suas reservas

Contexto: 
Dado tenho os seguintes quartos
| Andar | Número | Estrelas | Situação  |
| 1     | 101    | 5        | Vago      |
| 1     | 102    | 5        | Ocupado   |
| 2     | 201    | 4        | reservado |
| 2     | 202    | 4        | Ocupado   |
E as seguintes reservas
| Número | Cliente |
| 201    | Vanessa |
E que estou na tela inicial

Cenário: Deve remover a reserva de um quarto do hotel
Dado que sou um usuário com cargo de gerente ou de funcionário da recepção
E que acesso a tela de remover reserva de quarto
E informo nome do Cliente "Vanessa"
E é apresentada a mensagem de confirmação "Quarto 201. Deseja remover a reserva?"
Quando informo que sim
Então o quarto passa a ter o estado disponível
E um log de remoção de reserva de quarto é incluído
E volto para a tela inicial

Cenário: Deve cancelar a remoção da reserva de um quarto
Dado que sou um usuário com cargo de gerente ou de funcionário da recepção
E que acesso a tela de remover reserva de quarto
E informo nome do Cliente "Vanessa"
E é apresentada a mensagem de confirmação "Quarto 201. Deseja remover a reserva?"
Quando informo que não
Então o quarto continua com o estado de reservado 
E um log de remoção de reserva de quarto é incluído
E volto para a tela inicias

Cenário: Deve validar os campos obrigatórios para remover reserva de quarto
Dado que sou um usuário com cargo de gerente ou de funcionário da recepção
E que acesso a tela de finalizar o aluguel
E que não informo o nome do cliente
Quando terminar de preencher os campos
Então uma mensagem de validação é apresentada "O campo Cliente é obrigatório"
E um log de Finalização de aluguel é criado
E volta para a tela inicial

Cenário: Deve bloquear a entrada de usuário diferente de gerente ou funcionário da recepção na tela de remover reserva de quartos
Dado que não sou um usuário com cargo de gerente ou funcionário da recepção
Quando acesso a tela de remover reserva de quartos
Então uma mensagem de validação é apresentada "Para acessar essa página você deve ter o cargo de gerente ou de funcionário da recepção. Será redirecionado para a tela incial."
E volto para a tela inicial
E um log de aluguel de quarto é incluído
