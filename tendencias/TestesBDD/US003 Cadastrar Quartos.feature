﻿Funcionalidade: US003 Cadastrar Quartos
	Eu como gerente do hotel
	Quero cadastrar quartos
	Para poder alguar eles

Contexto:
Dado que fiz login no sistema
E que estou na tela inicial

Cenário: Deve cadastrar um apartemento no sistema
Dado que sou um usuário com cargo de Gerente
E que acesso a tela de cadastrar Apartementos
E que preencho todos os campos
Quando terminar de preencher os campos
Então o quarto é cadastrado
E o quarto terá status disponível
E volta para a tela inicial
E um log de cadastro de quarto é incluído

Esquema do Cenário:	Deve validar os campos obrigatórios para cadastrar quartos
Dado que sou um usuário com cargo de Gerente
E que acesso a tela de cadastrar quartos
E que não preencho o campo <campo>
Quando terminar de preencher os campos
Então uma mensagem de validação é apresentada <mensagem>
E volta para a tela inicial
E um log de cadastro de quarto é incluído
Exemplos: 
| campo           | mensagem                               |
| Andar           | O campo Andar é obrigatório.           |
| Número          | O campo Número é obrigatório.          |
| Estrelas        | O campo Estrelas é obrigatório.        |
| Valor da Diária | O campo Valor da Diária é obrigatório. |

Cenário: Deve bloquear a entrada de usuário diferente de Gerente na tela de cadastro de quartos
Dado que não sou um usuário com cargo de Gerente
Quando acesso a tela de cadasrtrar quartos
Então uma mensagem de validação é apresentada "Para acessar essa página você deve ter o cargo de gerente ou de funcionário da recepção. Será redirecionado para a tela incial."
E volto para a tela inicial
E um log de cadastro de usuário é incluído

