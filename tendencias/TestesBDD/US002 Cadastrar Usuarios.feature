﻿Funcionalidade: US002 Cadastrar Usuários
	Eu como gerente do hotel
	Quero cadastrar usuários no sistema
	Para que eles possam acessar os sistema

Contexto:
Dado que fiz login no sistema
E que estou na tale inicial

Cenário: Deve cadastrar um usuário no sistema
Dado que sou um usuário com cargo de gerente
E que acesso a tela de cadastrar usuários
E que preencho todos os campos
Quando terminar de preencher os campos
Então o usuário é cadastrado
E volta para a tela incial
E um log de cadastro de usuário é incluído

Esquema do Cenário:	Deve validar os campos obrigatórios para cadastrar usuários
Dado que sou um usuário com cargo de gerente
E que acesso a tela de cadastrar usuários
E que não preencho o campo <campo>
Quando terminar de preencher os campos
Então uma mensagem de validação é apresentada <mensagem>
E volta para a tela inicial
E um log de cadastro de usuário é incluído
Exemplos: 
| campo | mensagem                     |
| Nome  | O campo Nome é obrigatório.  |
| Login | O campo Login é obrigatório. |
| Senha | O campo Senha é obrigatório. |
| Cargo | O campo Cargo é obrigatório. |

Cenário: Deve bloquear a entrada de usuário diferente de gerente na tela de cadastro de usuários
Dado que não sou um usuário com cargo de gerente
Quando acesso a tela de cadasrtrar usuário
Então uma mensagem de validação é apresentada "Para acessar essa página você deve ter o cargo de gerente. Será redirecionado para a tela incial."
E volto para a tela inicial
E um log de cadastro de usuário é incluído