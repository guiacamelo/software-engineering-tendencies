﻿Funcionalidade: US005 Alugar Quartos
	Eu como gerente ou funcionário da recepção
	Quero alugar quartos
	Para que os clientes possam se hospedar no hotel

Contexto: 
Dado tenho os seguintes quartos
| Andar | Número | Estrelas | Situação  |
| 1     | 101    | 5        | Vago      |
| 1     | 102    | 5        | Ocupado   |
| 2     | 201    | 4        | reservado |
| 2     | 202    | 4        | Ocupado   |
E que estou na tela inicial

Cenário: Deve alugar um quarto do hotel
Dado que sou um usuário com cargo de gerente ou de funcionário da recepção
E que acesso a tela de aluguel de quarto
E que os quartos são listados
E que informo o nome do cliente
Quando seleciono o quarto 101
Então o quarto passa a ter a situação de ocupado
E a data de início do aluguel é a data atual
E uma mensagem de sucesso é apresentada "Quarto 101 alugado com sucesso."
E volta para a tela inicial
E um log de aluguel de quarto é incluído

Cenário: Deve alugar um quarto reservado
Dado que sou um usuário com cargo de gerente ou de funcionário da recepção
E que acesso a tela de reserva de quarto
E que reservo o quarto 101 para um cliente
E que acesso a tela de alugel de quarto
E que os quartos são listados
E que informo o nome do mesmo cliente
Quando seleciono o quarto 101
E uma mensagem de sucesso é apresentada "Quarto 101 alugado com sucesso."
E volta para a tela inicial
E um log de aluguel de quarto é incluído

Cenário: Deve impedir o aluguel de um quarto reservado
Dado que sou um usuário com cargo de gerente ou de funcionário da recepção
E que acesso a tela de aluguel de quarto
E que os quartos são listados
E que informo o nome do cliente
Quando seleciono o quarto 102 que está reservado para outro cliente
Então uma mensagem de validação é apresentada "Quarto já reservado."
E volta para a tela inicial
E um log de aluguel de quarto é incluído

Cenário: Deve impedir o aluguel de um quarto ocupado
Dado que sou um usuário com cargo de gerente ou de funcionário da recepção
E que acesso a tela de aluguel de quarto
E que os quartos são listados
E que informo o nome do cliente
Quando seleciono o quarto 102
Então uma mensagem de validação é apresentada "Quarto já ocupado."
E volta para a tela inicial
E um log de aluguel de quarto é incluído

Esquema do Cenário: Deve validar os campos obrigatórios para alugar um quarto
Dado que sou um usuário com cargo de gerente ou de funcionário da recepção
E que acesso a tela de aluguel de quarto
E que os quartos são listados
E que não informo o campo <campo>
Quando terminar de preencher os campos
Então uma mensagem de validação é apresentada <mensagem>
E volta para a tela inicial
E um log de aluguel de quarto é incluído
Exemplos: 
| campo      | mensagem                           |
| Cliente    | O campo Cliente é obrigatório.     |
| Apartamnto | O campo Apartamento é obrigatório. |

Cenário: Deve bloquear a entrada de usuário diferente de gerente ou funcionário da recepção na tela de aluguel de quartos
Dado que não sou um usuário com cargo de gerente ou funcionário da recepção
Quando acesso a tela de alugar quartos
Então uma mensagem de validação é apresentada "Para acessar essa página você deve ter o cargo de gerente ou de funcionário da recepção. Será redirecionado para a tela incial."
E volto para a tela inicial
E um log de aluguel de quarto é incluído
