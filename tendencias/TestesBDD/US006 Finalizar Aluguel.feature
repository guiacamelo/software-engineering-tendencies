﻿Funcionalidade: US006 Finalizar Aluguel
	Eu como usuário com cargo de gerente ou de funcionário da recepão
	Quero finalizar o aluguel dos clientes
	Para que eu possa fatura com isso

Contexto: 
Dado tenho os seguintes quartos
| Andar | Número | Estrelas | Situação  | Valor da Diária |
| 1     | 101    | 5        | Ocupado   | 50,00           |
| 1     | 102    | 5        | Ocupado   | 60,00           |
| 2     | 201    | 4        | Reservado | 35,00           |
| 2     | 202    | 4        | Vago      | 35,00           |
E os seguinta registros de aluguel
| Quarto | Cliente | Data de Início |
| 101    | Alberto | 2 dias atrás   |
| 102    | Otávio  | 5 dias atrás   |
E que estou na tela inicial

Cenário: Deve finalizar o aluguel
Dado que sou um usuário com cargo de gerente ou de funcionário da recepção
E que acesso a tela de finalizar o aluguel
E que informo o cliente "Otávio"
E é apresentada a mensagem de confirmação "Quarto 101 alguado desde (2 dias atrás). Valor R$100,00. Deseja finalizar o aluguel?" 
Quando informo que sim
Então a situação do quarto passa a ser Vago
E volta para a tela inicial do sistema
E um log de Finalização de aluguel é criado

Cenário: Deve cancelar a finalização o aluguel
Dado que sou um usuário com cargo de gerente ou de funcionário da recepção
E que acesso a tela de finalizar o aluguel
E que
E que informo o cliente "Otávio"
E que é apresentada a mensagem de confirmação "Quarto 101 alguado desde (2 dias atrás). Valor R$100,00. Deseja finalizar o aluguel?" 
Quando informo que não
Então a situação do quarto continua sendo Ocupado
E volta para a tela inicial do sistema
E um log de Finalização de aluguel é criado


Cenário: Deve validar os campos obrigatórios para finalizar o aluguel
Dado que sou um usuário com cargo de gerente ou de funcionário da recepção
E que acesso a tela de finalizar o aluguel
E que não informo o nome do cliente
Quando terminar de preencher os campos
Então uma mensagem de validação é apresentada "O campo Cliente é obrigatório"
E volta para a tela inicial
E um log de Finalização de aluguel é criado

Cenário: Deve bloquear a entrada de usuário diferente de gerente ou funcionário da recepção na tela de finalizar aluguel
Dado que não sou um usuário com cargo de gerente ou funcionário da recepção
Quando acesso a tela de finalizar aluguel
Então uma mensagem de validação é apresentada "Para acessar essa página você deve ter o cargo de gerente ou de funcionário da recepção. Será redirecionado para a tela incial."
E volto para a tela inicial
E um log de Finalização de aluguel é criado
