﻿using System;
using TendenciasAop.Negocio;

namespace TendenciasAop.Apresentacao
{
    public class Tela
    {
        public void ApresentarMenuPrincipal()
        {
            Console.Clear();
            Console.WriteLine("*** Menu Principal ***");
            Console.WriteLine();
            Console.WriteLine("1 - Cadastrar Usuário");
            Console.WriteLine("2 - Cadastrar Quarto");
            Console.WriteLine("3 - Reservar Quarto");
            Console.WriteLine("4 - Remover Reserva");
            Console.WriteLine("5 - Alugar Quarto");
            Console.WriteLine("6 - Finalizar Aluguel");
            Console.WriteLine("7 - Sair");
            Console.WriteLine();

            Console.Write("Opção: ");
            string opcao = Console.ReadLine();

            switch(opcao)
            {
                case "1":
                    InserirUsuario();
                    break;
                case "2":
                    InserirQuarto();
                    break;
                case "3":
                    InserirReservaQuarto();
                    break;
                case "4":
                    RemoverReserva();
                    break;
                case "5":
                    AlugarQuarto();
                    break;
                case "6":
                    FinalizarAluguel();
                    break;
                case "7":
                    Environment.Exit(1);
                    break;
                default:
                    Console.Clear();
                    Console.Write("*** Opção Inválida ***");
                    Console.ReadLine();
                    break;
            }
        }

        public void InserirUsuario()
        {
            Usuario usuario = new Usuario();
            Console.Clear();
            Console.WriteLine("*** Cadastrar Usuário ***");
            Console.WriteLine();
            Console.Write("Nome: ");
            usuario.Nome = Console.ReadLine();
            Console.Write("Login: ");
            usuario.Login = Console.ReadLine();
            Console.Write("Senha: ");
            usuario.Senha = Console.ReadLine();
            Console.Write("Cargo: ");
            usuario.Cargo = Console.ReadLine();
            usuario.Inserir();
        }

        public void InserirQuarto()
        {
            Quarto quarto = new Quarto();
            Console.Clear();
            Console.WriteLine("*** Cadastrar Quarto ***");
            Console.WriteLine();
            Console.Write("Andar: ");
            quarto.Andar = Console.ReadLine();
            Console.Write("Número: ");
            quarto.Numero = Console.ReadLine();
            Console.Write("Estrelas: ");
            quarto.Estrelas = Console.ReadLine();
            Console.Write("Valor da diária: ");
            quarto.ValorDiaria = Console.ReadLine();
            quarto.Inserir();
        }

        public void InserirReservaQuarto()
        {
            ReservaQuarto reserva = new ReservaQuarto();
            Console.Clear();
            Console.WriteLine("*** Reservar Quarto ***");
            Console.WriteLine();

            var quartos = Quarto.ListarQuartos();

            foreach(Quarto q in quartos)
            {
                Console.WriteLine(q.Andar + "º Andar - Número " + q.Numero + " - " + q.Status);
            }

            Console.WriteLine();
            Console.Write("Cliente: ");
            reserva.Cliente = Console.ReadLine();
            Console.Write("Quarto: ");
            reserva.NumeroQuarto = Console.ReadLine();
            
            reserva.Inserir();
        }

        public void AlugarQuarto()
        {
            AluguelQuarto aluguel = new AluguelQuarto();
            Console.Clear();
            Console.WriteLine("*** Alugar Quarto");
            Console.WriteLine();

            var quartos = Quarto.ListarQuartos();

            foreach (Quarto q in quartos)
            {
                Console.WriteLine(q.Andar + "º Andar - Número " + q.Numero + " - " + q.Status);
            }

            Console.WriteLine();
            Console.Write("Cliente: ");
            aluguel.Cliente = Console.ReadLine();
            Console.Write("Quarto: ");
            aluguel.NumeroQuarto  = Console.ReadLine();

            aluguel.Inserir();
        }

        public void RemoverReserva()
        {
            string nome = string.Empty;
            ReservaQuarto reserva = new ReservaQuarto();
            Console.Clear();
            Console.WriteLine("*** Remover Reserva ***");
            Console.WriteLine();
            Console.Write("Cliente: ");
            nome = Console.ReadLine();
            reserva.Remover(nome);
        }

        public Usuario FazerLogin()
        {
            Login login = new Login();
            Console.Clear();
            Console.WriteLine("*** Olá, bem vindo ao sistema. ***");
            Console.WriteLine();
            Console.Write("Login: ");
            login.Nome = Console.ReadLine();
            Console.Write("Senha: ");
            login.Senha = Console.ReadLine();
            return login.ValidarLogin();
        }

        public void FinalizarAluguel()
        {
            AluguelQuarto aluguelQuarto = new AluguelQuarto();
            Console.Clear();
            Console.WriteLine("*** Finalizar Aluguel ***");
            Console.WriteLine();
            Console.Write("Cliente: ");
            aluguelQuarto.Cliente = Console.ReadLine();
            aluguelQuarto.FinalizarAluguel(aluguelQuarto.Cliente );
        }
    }
}
