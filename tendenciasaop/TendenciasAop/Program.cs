﻿using TendenciasAop.Apresentacao;
using TendenciasAop.Dados;
using TendenciasAop.Negocio;

namespace TendenciasAop
{
    class Program
    {
        public static Usuario UsuarioSessao { get; set; }
        static void Main(string[] args)
        {
            Banco.IniciaBanco();
            Tela tela = new Tela();
            do
            {
                tela.FazerLogin();
            } while (Program.UsuarioSessao == null);

            do
            {
                tela.ApresentarMenuPrincipal();
            } while (true);
            
        }
    }
}
