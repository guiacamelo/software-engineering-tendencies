﻿using System.Collections.Generic;
using System.Linq;
using TendenciasAop.Negocio;

namespace TendenciasAop.Dados
{
    public class DaoAluguelQuarto
    {

        public void Inserir(AluguelQuarto aluguelQuarto)
        {
            Banco.AlugueisQuarto.Add(aluguelQuarto);
        }

        public List<AluguelQuarto> ListaAluguelQuartos()
        {
            return Banco.AlugueisQuarto.ToList();
        }

        public AluguelQuarto Consultar(string numeroQuarto)
        {
            return Banco.AlugueisQuarto.SingleOrDefault(x => x.NumeroQuarto == numeroQuarto);
        }
        public AluguelQuarto ConsultarPorNome(string nomeCliente)
        {
            return Banco.AlugueisQuarto.SingleOrDefault(x => x.Cliente == nomeCliente);
        }

        public void Remover(AluguelQuarto  aluguelQuarto)
        {
            Banco.AlugueisQuarto.Remove(aluguelQuarto);
        }
    }
}