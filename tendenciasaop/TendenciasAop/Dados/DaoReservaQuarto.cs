﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TendenciasAop.Negocio;

namespace TendenciasAop.Dados
{
    public class DaoReservaQuarto
    {
        public void Inserir(ReservaQuarto reservaQuarto)
        {
            Banco.ReservasQuarto.Add(reservaQuarto);
        }

        public ReservaQuarto ConsultarPorNome(string nomeCliente)
        {
            return Banco.ReservasQuarto.SingleOrDefault(x => x.Cliente == nomeCliente);
        }

        public void Remover(ReservaQuarto reservaQuarto)
        {
            Banco.ReservasQuarto.Remove(reservaQuarto);
        }
        
    }
}
