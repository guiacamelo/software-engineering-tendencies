﻿using System.Collections.Generic;
using TendenciasAop.Negocio;

namespace TendenciasAop.Dados
{
    public static class Banco
    {
        public static void IniciaBanco()
        {
            Usuarios = new List<Usuario>()
            {
                new Usuario() {Nome = "João da Silva", Login = "joao", Senha = "joao", Cargo = "Gerente" },
                new Usuario() {Nome = "Francisco", Login = "fran", Senha = "fran", Cargo = "Recepcao" },
            };
            Quartos = new List<Quarto>()
            {
                new Quarto() {Andar = "1", Numero="1",Estrelas="1",ValorDiaria="1" }
            };
            ReservasQuarto = new List<ReservaQuarto>();
            AlugueisQuarto = new List<AluguelQuarto>();
        }
        public static List<Usuario> Usuarios;
        public static List<Quarto> Quartos;
        public static List<ReservaQuarto> ReservasQuarto;
        public static List<AluguelQuarto> AlugueisQuarto;
    }
}
