﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TendenciasAop.Negocio;

namespace TendenciasAop.Dados
{
    public class DaoUsuario
    {
        public void Inserir(Usuario usuario)
        {
            Banco.Usuarios.Add(usuario);
        }

        public Usuario ValidarLogin(Login login)
        {
            return Banco.Usuarios.SingleOrDefault(x => x.Login == login.Nome && x.Senha == login.Senha);
        }
    }
}
