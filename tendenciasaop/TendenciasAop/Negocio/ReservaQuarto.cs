﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TendenciasAop.Dados;

namespace TendenciasAop.Negocio
{
    public class ReservaQuarto
    {
        public static string Mensagem;
        public static ReservaQuarto ReservaQuartoInserida;
        public static ReservaQuarto ReservaQuartoRemovida;

        public ReservaQuarto()
        {
        }

        public string Cliente { get; set; }
        public string NumeroQuarto { get; set; }
        public Quarto Quarto { get; set; }

        public void Inserir()
        {
            if(String.IsNullOrEmpty(this.Cliente))
            {
                Mensagem = "O campo Cliente é obrigatório.";
            }

            if(String.IsNullOrEmpty(this.NumeroQuarto))
            {
                Mensagem = "O campo Número é obrigatório.";
            }
            else
            {
                var quarto = Quarto.Consultar(this.NumeroQuarto);
                if(quarto == null)
                {
                    Mensagem = "Número de quarto inválido.";
                }
                else
                {
                    quarto.Status = "Reservado";
                }
                this.Quarto = quarto;
            }

            ReservaQuartoInserida = this;

            if(String.IsNullOrEmpty(Mensagem))
            {
                DaoReservaQuarto dao = new DaoReservaQuarto();
                dao.Inserir(this);
            }
        }

        public string RegistraLogCadastroReservaQuartoInserida(bool inseriu)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("Tentativa de cadastrar quarto ");
            builder.Append(DateTime.Now);
            builder.Append(Environment.NewLine);
            builder.Append("Status: ");
            builder.Append(inseriu ? "Válido" : "Inválido");
            builder.Append(Environment.NewLine);
            builder.Append("Cliente: ");
            builder.Append(this.Cliente);
            builder.Append(Environment.NewLine);
            builder.Append("Número: ");
            builder.Append(this.Quarto.Numero);
            builder.Append(Environment.NewLine);
            builder.Append(Environment.NewLine);
            builder.Append(string.Empty.PadRight(100, '-'));
            builder.Append(Environment.NewLine);
            builder.Append(Environment.NewLine);
            return builder.ToString();
        }

        public string RegistraLogCadastroReservaQuartoSemPermissao()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("Tentativa de cadastrar quarto ");
            builder.Append(DateTime.Now);
            builder.Append(Environment.NewLine);
            builder.Append("Status: ");
            builder.Append("Sem permissão");
            builder.Append(Environment.NewLine);
            builder.Append(Environment.NewLine);
            builder.Append(string.Empty.PadRight(100, '-'));
            builder.Append(Environment.NewLine);
            builder.Append(Environment.NewLine);
            return builder.ToString();
        }

        public void Remover(string cliente)
        {
            string opcao;
            if (String.IsNullOrEmpty(cliente))
            {
                Mensagem = "O campo Cliente é obrigatório.";
            }
            else
            {
                var reserva = ConsultarPorNomeCliente(cliente);
                if (reserva == null)
                {
                    Mensagem = "*** Cliente inválido. ***";
                }
                else
                {
                    this.Quarto = reserva.Quarto;
                    Console.Write("S para sim, N para não: ");
                    opcao = Console.ReadLine();
                    if (!string.IsNullOrEmpty(opcao )&&  opcao.ToUpper().Equals("S"))
                    {
                        this.Quarto.Status = "Disponível";
                    }
                    else
                    {
                        Mensagem = " Remoção de Reserva Cancelada";
                    }
                }
            }

            ReservaQuartoRemovida = this;

            if (String.IsNullOrEmpty(Mensagem))
            {
                DaoReservaQuarto dao = new DaoReservaQuarto();
                dao.Remover(this);
            }
        }


        public  ReservaQuarto ConsultarPorNomeCliente(string nomeCliente)
        {
            DaoReservaQuarto dao = new DaoReservaQuarto();
            return dao.ConsultarPorNome(nomeCliente);
        }

        public string RegistraLogReservaQuartoRemovida(bool removeu)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("Tentativa de remover reserva de quarto ");
            builder.Append(DateTime.Now);
            builder.Append(Environment.NewLine);
            builder.Append("Status: ");
            builder.Append(removeu ? "Válido" : "Inválido");
            builder.Append(Environment.NewLine);
            builder.Append("Cliente: ");
            builder.Append(this.Cliente);
            builder.Append(Environment.NewLine);
            builder.Append("Número: ");
            builder.Append(this.Quarto.Numero);
            builder.Append(Environment.NewLine);
            builder.Append(Environment.NewLine);
            builder.Append(string.Empty.PadRight(100, '-'));
            builder.Append(Environment.NewLine);
            builder.Append(Environment.NewLine);
            return builder.ToString();
        }

        public string RegistraLogReservaQuartoRemovidaSemPermissao()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("Tentativa de remover reserva de quarto ");
            builder.Append(DateTime.Now);
            builder.Append(Environment.NewLine);
            builder.Append("Status: ");
            builder.Append("Sem permissão");
            builder.Append(Environment.NewLine);
            builder.Append(Environment.NewLine);
            builder.Append(string.Empty.PadRight(100, '-'));
            builder.Append(Environment.NewLine);
            builder.Append(Environment.NewLine);
            return builder.ToString();
        }
    }
}
