﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TendenciasAop.Dados;

namespace TendenciasAop.Negocio
{
    public class Login
    {
        public static Login TentativaLogin { get; set; }
        public string Nome { get; set; }
        public string Senha { get; set; }

        public Usuario ValidarLogin()
        {
            DaoUsuario dao = new DaoUsuario();
            TentativaLogin = this;
            return dao.ValidarLogin(this);
        }

        public string RegistrarLogTentativaLogin(bool sucesso)
        {
            StringBuilder builderLog = new StringBuilder();
            builderLog.Append("Tentativa de login ");
            builderLog.Append(DateTime.Now);
            builderLog.Append(Environment.NewLine);
            builderLog.Append("Status: ");
            builderLog.Append(sucesso ? "Válido" : "Inválido");
            builderLog.Append(Environment.NewLine);
            builderLog.Append("Nome: ");
            builderLog.Append(this.Nome);
            builderLog.Append(Environment.NewLine);
            builderLog.Append("Senha: ");
            builderLog.Append(this.Senha);
            builderLog.Append(Environment.NewLine);
            builderLog.Append(Environment.NewLine);
            builderLog.Append(string.Empty.PadRight(100, '-'));
            builderLog.Append(Environment.NewLine);
            builderLog.Append(Environment.NewLine);
            return builderLog.ToString();
        }
    }
}
