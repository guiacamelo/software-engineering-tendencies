﻿using System;
using System.Text;
using TendenciasAop.Dados;

namespace TendenciasAop.Negocio
{
    public class AluguelQuarto
    {
        public static AluguelQuarto QuartoAlugado;
        public static AluguelQuarto AluguelFinalizado;
        public static string Mensagem;
        public AluguelQuarto()
        {
            Random random = new Random();
            this.DataInicio = DateTime.Now.AddDays((random.Next(1, 10))*-1);
        }

        public string Cliente { get; set; }
        public string NumeroQuarto { get; set; }
        public Quarto Quarto { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }

        public void Inserir()
        {
            DaoAluguelQuarto dao = new DaoAluguelQuarto();

            if (String.IsNullOrEmpty(this.Cliente))
            {
                Mensagem = "O campo Cliente é obrigatório.";
            }

            if (this.NumeroQuarto == "-1")
            {
                 Mensagem ="O campo Número do apartamento é obrigatório.";
            }

            this.Quarto = Quarto.Consultar(this.NumeroQuarto);

            if (this.Quarto == null)
            {
                 Mensagem = "O número do quarto selecionado é inválido.";
            }
            else
            {
                if (this.Quarto.Status == "Ocupado")
                {
                     Mensagem = "O quarto " + this.NumeroQuarto.ToString() + " está ocupado.";
                }
                else if (this.Quarto.Status == "Reservado")
                {
                    var reserva = ReservaQuarto.ReservaQuartoInserida.ConsultarPorNomeCliente(this.Cliente);
                    if (this.Cliente != reserva.Cliente)
                         Mensagem = "O quarto " + this.NumeroQuarto.ToString() + " está reservado.";
                }
            }
            if (Quarto == null) return;
            QuartoAlugado = this;
            this.Quarto.AlugarQuarto();
            dao.Inserir(this);
        }

        public string RegistraLogAluguelQuartoSemPermissao()
        {
            StringBuilder builderLog = new StringBuilder();
            builderLog.Append("Tentativa de cadastro de aluguel de quato ");
            builderLog.Append(DateTime.Now);
            builderLog.Append(Environment.NewLine);
            builderLog.Append("Status: ");
            builderLog.Append("Sem permissão");
            builderLog.Append(Environment.NewLine);
            builderLog.Append(Environment.NewLine);
            builderLog.Append(string.Empty.PadRight(100, '-'));
            builderLog.Append(Environment.NewLine);
            builderLog.Append(Environment.NewLine);
            return builderLog.ToString();
        }

        public string RegistraLogAluguelQuarto(bool sucesso)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("Tentativa de cadastro de aluguel de quarto ");
            builder.Append(DateTime.Now);
            builder.Append(Environment.NewLine);
            builder.Append("Status: ");
            builder.Append(sucesso ? "Válido" : "Inválido");
            builder.Append(Environment.NewLine);
            builder.Append("Cliente: ");
            builder.Append(this.Cliente);
            builder.Append(Environment.NewLine);
            builder.Append("Quarto: ");
            builder.Append(NumeroQuarto);
            builder.Append(Environment.NewLine);
            builder.Append("Data de Início: ");
            builder.Append(DataInicio);
            builder.Append(Environment.NewLine);
            builder.Append(Environment.NewLine);
            builder.Append(String.Empty.PadRight(100, '-'));
            builder.Append(Environment.NewLine);
            builder.Append(Environment.NewLine);
            return builder.ToString();
        }
        
        public static AluguelQuarto Consultar(string nomeCliente)
        {
            DaoAluguelQuarto dao = new DaoAluguelQuarto();

            return dao.ConsultarPorNome(nomeCliente);
        }

        public void FinalizarAluguel(string cliente)
        {
            string opcao;
            if (string.IsNullOrEmpty(cliente))
            {
                Mensagem = "Campo cliente é obrigatório";
            }
            else
            {
                var aluguel = Consultar(cliente);

                if (aluguel  == null)
                {
                    Mensagem = "*** Cliente inválido. ***";
                }
                else
                {
                    this.Quarto = aluguel.Quarto;

                    TimeSpan date = DateTime.Today - this.DataInicio.Date;
                    int diferencaDias = date.Days;
                    decimal valorFinal = diferencaDias * int.Parse( this.Quarto.ValorDiaria);
                    
                    Console.Write(String.Format("Quarto alugado desde {0}. Valor R${1}. Deseja finalizar o alguel? ", this.DataInicio, valorFinal));
                    opcao = Console.ReadLine();
                    if (!string.IsNullOrEmpty( opcao ) &&  opcao.ToUpper().Equals("S"))
                    {
                        this.Quarto.Status = "Disponível";
                        this.DataFim = DateTime.Now;
                    }
                    else
                    {
                        Mensagem = " Finalização do Aluguel Cancelada";
                    }
                }
            }
            AluguelFinalizado = this;

            if (String.IsNullOrEmpty(Mensagem))
            {
                DaoAluguelQuarto  dao = new DaoAluguelQuarto();
                dao.Remover(this);
            }
        }

        public  string RegistraLogFinalizarAluguelQuarto(bool finalizou)
        {
            StringBuilder builder = new StringBuilder();

            builder.Append("Tentativa de finalização do alguel ");
            builder.Append(DateTime.Now);
            builder.Append(Environment.NewLine);
            builder.Append("Quarto: ");
            builder.Append(this.Quarto.Numero);
            builder.Append(Environment.NewLine);
            builder.Append(finalizou ? "Finalizado" : "Não finalizado");
            builder.Append(Environment.NewLine);
            builder.Append(Environment.NewLine);
            builder.Append(string.Empty.PadRight(100, '-'));
            builder.Append(Environment.NewLine);
            builder.Append(Environment.NewLine);
            return builder.ToString();
        }

        public string RegistraLogFinalizarAluguelQuartoSemPermissao()
        {
            StringBuilder builderLog = new StringBuilder();
            builderLog.Append("Tentativa de cadastro de aluguel de quato ");
            builderLog.Append(DateTime.Now);
            builderLog.Append(Environment.NewLine);
            builderLog.Append("Status: ");
            builderLog.Append("Sem permissão");
            builderLog.Append(Environment.NewLine);
            builderLog.Append(Environment.NewLine);
            builderLog.Append(string.Empty.PadRight(100, '-'));
            builderLog.Append(Environment.NewLine);
            builderLog.Append(Environment.NewLine);
            return builderLog.ToString();
        }

     }
}