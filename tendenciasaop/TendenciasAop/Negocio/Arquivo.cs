﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TendenciasAop.Negocio
{
    public static class Arquivo
    {
        public static void RegistrarLog(string texto, string arquivo)
        {
            if (File.Exists(arquivo))
            {
                var streamWriter = new StreamWriter(arquivo, true);
                streamWriter.WriteLine(texto);
                streamWriter.Flush();
                streamWriter.Close();
            }
            else
            {
                File.WriteAllText(arquivo, texto);
            }
        }
    }
}
