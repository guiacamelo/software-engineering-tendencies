﻿using SheepAspect.Framework;
using SheepAspect.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TendenciasAop.Apresentacao;

namespace TendenciasAop.Negocio.Aspectos
{
    [Aspect]
    public class UsuarioAspecto
    {
        [SelectMethods("'* TendenciasAop.Apresentacao.Tela::InserirUsuario()'")]
        public void PointCutInserirUsuario() { }

        [Around("PointCutInserirUsuario")]
        public object InterceptMethod(MethodJointPoint jp)
        {
            string result = string.Empty;
            if (Program.UsuarioSessao.Cargo == "Gerente")
            {
                result = (string)jp.Execute();
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Usuário sem permissão");
                Arquivo.RegistrarLog(Usuario.RegistraLogCadastroUsuarioSemPermissao(), @"LogCadastrarUsuario.txt");
                return null;

            }
            
            if (String.IsNullOrEmpty(Usuario.Mensagem))
            {
                Console.Clear();
                Console.WriteLine("Usuário Inserido com sucesso.");
                Arquivo.RegistrarLog(Usuario.UsuarioInserido.RegistraLogCadastroUsuarioInserido(true), @"LogCadastrarUsuario.txt");
            }
            else
            {
                Console.Clear();
                Console.WriteLine(Usuario.Mensagem);
                Arquivo.RegistrarLog(Usuario.UsuarioInserido.RegistraLogCadastroUsuarioInserido(false), @"LogCadastrarUsuario.txt");
            }

            return null;
        }
    }
}

//Console.WriteLine("Caiu no aspecto");
//            bool result = false;
//            if (Program.usuarioSessao.Cargo == "Gerente")
//            {
//                result = (bool)jp.Execute();
//            }
//            else
//            {
//                Usuario.Resultado = Constantes.SEM_PERMISSAO;
//                Console.WriteLine("Usuário sem permissão");

//            }
//            Console.WriteLine(result);
//            Console.ReadLine();
//            if(result)
//            {

//                Usuario.Resultado = Constantes.INSERIU;
//                Arquivo.RegistrarLog("Inseriu", @"LogCadastrarUsuario.txt");
//            }
//            else
//            {
//                Arquivo.RegistrarLog("Não inseriu", @"LogCadastrarUsuario.txt");
//            }

//            return null;



