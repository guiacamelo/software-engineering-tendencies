﻿using SheepAspect.Framework;
using SheepAspect.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TendenciasAop.Negocio.Aspectos
{
    [Aspect]
    public class LoginAspecto
    {
        [SelectMethods("'* TendenciasAop.Apresentacao.Tela::FazerLogin()'")]
        public void PointCutFazerLogin() { }

        [Around("PointCutFazerLogin")]
        public void InterceptMethod(MethodJointPoint jp)
        {
            Program.UsuarioSessao = (Usuario)jp.Execute();

            if(Program.UsuarioSessao == null)
            {
                Console.WriteLine("*** Login ou senha inválidos ***");
                Console.ReadLine();
                Arquivo.RegistrarLog(Login.TentativaLogin.RegistrarLogTentativaLogin(false), @"LogLogin.txt");
            }
            else
            {
                Arquivo.RegistrarLog(Login.TentativaLogin.RegistrarLogTentativaLogin(true), @"LogLogin.txt");
            }
        }
    }
}
