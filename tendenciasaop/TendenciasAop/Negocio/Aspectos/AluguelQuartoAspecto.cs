﻿using System;
using SheepAspect.Framework;
using SheepAspect.Runtime;

namespace TendenciasAop.Negocio.Aspectos
{
    [Aspect]
    public class AluguelQuartoAspecto
    {
        [SelectMethods("'* TendenciasAop.Apresentacao.Tela::AlugarQuarto()'")]
        public void PointCutAlugarQuarto() { }

        [Around("PointCutAlugarQuarto")]
        public object InterceptMethodAlugar(MethodJointPoint jp)
        {
            string result = string.Empty;
            if (Program.UsuarioSessao.Cargo == "Gerente" || Program.UsuarioSessao.Cargo == "Recepção")
            {
                result = (string)jp.Execute();
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Usuário sem permissão");
                Arquivo.RegistrarLog(AluguelQuarto.QuartoAlugado.RegistraLogAluguelQuartoSemPermissao(), @"LogAlugarQuarto.txt");
                return null;
            }

            if (String.IsNullOrEmpty(AluguelQuarto.Mensagem))
            {
                Console.Clear();
                Console.WriteLine("Quarto inserido com sucesso.");
                Arquivo.RegistrarLog(AluguelQuarto.QuartoAlugado.RegistraLogAluguelQuarto(true), @"LogAlugarQuarto.txt");
            }
            else
            {
                Console.Clear();
                Console.WriteLine(AluguelQuarto.Mensagem);
                Arquivo.RegistrarLog(AluguelQuarto.QuartoAlugado.RegistraLogAluguelQuarto(false), @"LogAlugarQuarto.txt");
            }

            return null;
        }

        #region PointCutFinalizar

        [SelectMethods("'* TendenciasAop.Apresentacao.Tela::FinalizarAluguel()'")]
        public void PointCutFinalizarAluguel() { }

        [Around("PointCutFinalizarAluguel")]
        public object InterceptMethodFinalizar(MethodJointPoint jp)
        {
            string result = string.Empty;
            if (Program.UsuarioSessao.Cargo == "Gerente" || Program.UsuarioSessao.Cargo == "Recepção")
            {
                result = (string)jp.Execute();
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Usuário sem permissão");
                Arquivo.RegistrarLog(AluguelQuarto.AluguelFinalizado.RegistraLogFinalizarAluguelQuartoSemPermissao(), @"LogFinalizarAluguelQuarto.txt");
                return null;
            }

            if (String.IsNullOrEmpty(AluguelQuarto.Mensagem))
            {
                Console.Clear();
                Console.WriteLine("Aluguel finalizado com sucesso.");
                Arquivo.RegistrarLog(AluguelQuarto.AluguelFinalizado.RegistraLogFinalizarAluguelQuarto(true), @"LogFinalizarAluguelQuarto.txt");
            }
            else
            {
                Console.Clear();
                Console.WriteLine(AluguelQuarto.Mensagem);
                Arquivo.RegistrarLog(AluguelQuarto.AluguelFinalizado.RegistraLogFinalizarAluguelQuarto(false), @"LogFinalizarAluguelQuarto.txt");
            }

            return null;
        }

        #endregion
    }
}