﻿using SheepAspect.Framework;
using SheepAspect.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TendenciasAop.Negocio.Aspectos
{
    [Aspect]
    public class ReservaQuartoAspecto
    {
        [SelectMethods("'* TendenciasAop.Apresentacao.Tela::InserirReservaQuarto()'")]
        public void PointCutInserirReservaQuarto() { }

        [Around("PointCutInserirReservaQuarto")]
        public object InterceptMethodInserirQuarto(MethodJointPoint jp)
        {
            string result = string.Empty;
            if (Program.UsuarioSessao.Cargo == "Gerente" || Program.UsuarioSessao.Cargo == "Recepção")
            {
                result = (string)jp.Execute();
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Usuário sem permissão");
                ReservaQuarto.ReservaQuartoInserida = new ReservaQuarto();
                Arquivo.RegistrarLog(ReservaQuarto.ReservaQuartoInserida.RegistraLogCadastroReservaQuartoSemPermissao(), @"LogCadastrarReservaQuarto.txt");
                return null;
            }

            if (String.IsNullOrEmpty(ReservaQuarto.Mensagem))
            {
                Console.Clear();
                Console.WriteLine("Reserva inserido com sucesso.");
                Arquivo.RegistrarLog(ReservaQuarto.ReservaQuartoInserida.RegistraLogCadastroReservaQuartoInserida(true), @"LogCadastrarReservaQuarto.txt");
            }
            else
            {
                Console.Clear();
                Console.WriteLine(ReservaQuarto.Mensagem);
                Arquivo.RegistrarLog(ReservaQuarto.ReservaQuartoInserida.RegistraLogCadastroReservaQuartoInserida(false), @"LogCadastrarReservaQuarto.txt");
            }

            return null;
        }

        [SelectMethods("'* TendenciasAop.Apresentacao.Tela::RemoverReserva()'")]
        public void PointCutRemoverReserva() { }

        [Around("PointCutRemoverReserva")]
        public object InterceptMethodRemoverReserva(MethodJointPoint jp)
        {
            string result = string.Empty;
            if (Program.UsuarioSessao.Cargo == "Gerente" || Program.UsuarioSessao.Cargo == "Recepção")
            {
                result = (string)jp.Execute();
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Usuário sem permissão");
                ReservaQuarto.ReservaQuartoRemovida = new ReservaQuarto();
                Arquivo.RegistrarLog(ReservaQuarto.ReservaQuartoRemovida.RegistraLogReservaQuartoRemovidaSemPermissao(), @"LogRemoverReservaQuarto.txt");
                return null;
            }

            if (String.IsNullOrEmpty(ReservaQuarto.Mensagem))
            {
                Console.Clear();
                Console.WriteLine("Reserva removida com sucesso.");
                Arquivo.RegistrarLog(ReservaQuarto.ReservaQuartoRemovida.RegistraLogReservaQuartoRemovida(true), @"LogRemoverReservaQuarto.txt");
            }
            else
            {
                Console.Clear();
                Console.WriteLine(ReservaQuarto.Mensagem);
                Arquivo.RegistrarLog(ReservaQuarto.ReservaQuartoRemovida.RegistraLogReservaQuartoRemovida(false), @"LogRemoverReservaQuarto.txt");
            }

            return null;
        }
    }
}
