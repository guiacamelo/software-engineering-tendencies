﻿using SheepAspect.Framework;
using SheepAspect.Runtime;
using System;

namespace TendenciasAop.Negocio.Aspectos
{
    [Aspect]
    public class QuartoAspecto
    {
        [SelectMethods("'* TendenciasAop.Apresentacao.Tela::InserirQuarto()'")]
        public void PointCutInserirQuarto() { }

        [Around("PointCutInserirQuarto")]
        public object InterceptMethod(MethodJointPoint jp)
        {
            string result = string.Empty;
            if (Program.UsuarioSessao.Cargo == "Gerente" || Program.UsuarioSessao.Cargo == "Recepção")
            {
                result = (string)jp.Execute();
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Usuário sem permissão");
                Arquivo.RegistrarLog(Quarto.RegistraLogCadastroUsuarioSemPermissao(), @"LogCadastrarQuarto.txt");
                return null;
            }

            if (String.IsNullOrEmpty(Quarto.Mensagem))
            {
                Console.Clear();
                Console.WriteLine("Quarto alugado com sucesso.");
                Arquivo.RegistrarLog(Quarto.QuartoInserido.RegistraLogCadastroQuartoInserido(true), @"LogCadastrarQuarto.txt");
            }
            else
            {
                Console.Clear();
                Console.WriteLine(Quarto.Mensagem);
                Arquivo.RegistrarLog(Quarto.QuartoInserido.RegistraLogCadastroQuartoInserido(false), @"LogCadastrarQuarto.txt");
            }

            return null;
        }
    }
}
