﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TendenciasAop.Dados;

namespace TendenciasAop.Negocio
{
    public class Usuario
    {
        public static Usuario UsuarioInserido { get; set; }
        public static string Mensagem { get; set; }

        public Usuario()
        {

        }

        public string Nome { get; set; }
        public string Login { get; set; }
        public string Senha { get; set; }
        public string Cargo { get; set; }

        public string Inserir()
        {
            string mensagem = string.Empty;
            DaoUsuario dao = new DaoUsuario();
            if(String.IsNullOrEmpty(this.Nome))
            {
                mensagem = "O campo Nome é obrigatório.";
            }
            if (String.IsNullOrEmpty(this.Login))
            {
                mensagem = "O campo Login é obrigatório.";
            }

            if(String.IsNullOrEmpty(this.Senha))
            {
                mensagem = "O campo Senha é obrigatório.";
            }

            if(String.IsNullOrEmpty(this.Cargo))
            {
                mensagem = "O campo Cargo é obrigatório.";
            }

            UsuarioInserido = this;
            dao.Inserir(this);
            Mensagem = mensagem;
            return mensagem;
        }

        public string RegistraLogCadastroUsuarioInserido(bool inseriu)
        {
            StringBuilder builderLog = new StringBuilder();
            builderLog.Append("Tentativa de cadastro de usuário ");
            builderLog.Append(DateTime.Now);
            builderLog.Append(Environment.NewLine);
            builderLog.Append("Status: ");
            builderLog.Append(inseriu ? "Válido" : "Inválido");
            builderLog.Append(Environment.NewLine);
            builderLog.Append("Nome: ");
            builderLog.Append(this.Nome);
            builderLog.Append(Environment.NewLine);
            builderLog.Append("Login: ");
            builderLog.Append(this.Login);
            builderLog.Append(Environment.NewLine);
            builderLog.Append("Senha: ");
            builderLog.Append(this.Senha);
            builderLog.Append(Environment.NewLine);
            builderLog.Append("Cargo: ");
            builderLog.Append(this.Cargo == null ? string.Empty : this.Cargo.ToString());
            builderLog.Append(Environment.NewLine);
            builderLog.Append(Environment.NewLine);
            builderLog.Append(string.Empty.PadRight(100, '-'));
            builderLog.Append(Environment.NewLine);
            builderLog.Append(Environment.NewLine);
            return builderLog.ToString();
        }

        public static string RegistraLogCadastroUsuarioSemPermissao()
        {
            StringBuilder builderLog = new StringBuilder();
            builderLog.Append("Tentativa de cadastro de usuário ");
            builderLog.Append(DateTime.Now);
            builderLog.Append(Environment.NewLine);
            builderLog.Append("Status: ");
            builderLog.Append("Usuário sem permissão");
            builderLog.Append(Environment.NewLine);
            builderLog.Append(Environment.NewLine);
            builderLog.Append(string.Empty.PadRight(100, '-'));
            builderLog.Append(Environment.NewLine);
            builderLog.Append(Environment.NewLine);
            return builderLog.ToString();
        }
    }
}
