﻿using System;
using System.Collections.Generic;
using System.Text;
using TendenciasAop.Dados;

namespace TendenciasAop.Negocio
{
    public class Quarto
    {
        public static string Mensagem;
        public static Quarto QuartoInserido;

        public Quarto()
        {
            this.Status = "Disponível";
        }

        public string Andar { get; set; }
        public string Numero { get; set; }
        public string Estrelas { get; set; }
        public string ValorDiaria { get; set; }
        public string Status { get; set; }

        public void Inserir()
        {
            if (String.IsNullOrEmpty(this.Andar))
            {
                Mensagem = "O campo Andar é obrigatório.";
            }

            if (String.IsNullOrEmpty(this.Numero))
            {
                Mensagem = "O campo Número é obrigatório.";
            }

            if (String.IsNullOrEmpty(this.Estrelas))
            {
                Mensagem = "O campo Estrelas é obrigatório.";
            }

            if (String.IsNullOrEmpty(this.ValorDiaria))
            {
                Mensagem = "O campo Valor Diária é obrigatório.";
            }

            QuartoInserido = this;
            if (String.IsNullOrEmpty(Mensagem))
            {
                DaoQuarto dao = new DaoQuarto();
                dao.Inserir(this);
            }
        }

        public static Quarto Consultar(string numeroQuarto)
        {
            DaoQuarto dao = new DaoQuarto();
            return dao.Consultar(numeroQuarto);
        }

        public string RegistraLogCadastroQuartoInserido(bool inseriu)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("Tentativa de cadastrar quarto ");
            builder.Append(DateTime.Now);
            builder.Append(Environment.NewLine);
            builder.Append("Status: ");
            builder.Append(inseriu ? "Válido" : "Inválido");
            builder.Append(Environment.NewLine);
            builder.Append("Andar: ");
            builder.Append(this.Andar);
            builder.Append(Environment.NewLine);
            builder.Append("Número: ");
            builder.Append(this.Numero);
            builder.Append(Environment.NewLine);
            builder.Append("Estrelas: ");
            builder.Append(this.Estrelas);
            builder.Append(Environment.NewLine);
            builder.Append("Valor Diária: ");
            builder.Append(this.ValorDiaria);
            builder.Append(Environment.NewLine);
            builder.Append(Environment.NewLine);
            builder.Append(string.Empty.PadRight(100, '-'));
            builder.Append(Environment.NewLine);
            builder.Append(Environment.NewLine);
            return builder.ToString();
        }

        public static List<Quarto> ListarQuartos()
        {
            DaoQuarto dao = new DaoQuarto();
            return dao.ListarQuartos();
        }

        public static string RegistraLogCadastroUsuarioSemPermissao()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("Tentativa de cadastrar quarto ");
            builder.Append(DateTime.Now);
            builder.Append(Environment.NewLine);
            builder.Append("Status: ");
            builder.Append("Sem permissão");
            builder.Append(Environment.NewLine);
            builder.Append(Environment.NewLine);
            builder.Append(string.Empty.PadRight(100, '-'));
            builder.Append(Environment.NewLine);
            builder.Append(Environment.NewLine);
            return builder.ToString();
        }

        public void AlugarQuarto()
        {
            this.Status = "Ocupado";
        }
    }
}
